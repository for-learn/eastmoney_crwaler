# 东方财富资金流向爬虫

#### 参考
```javascript
* https://blog.csdn.net/gzw12138/article/details/103704652
* https://blog.csdn.net/qq_41335232/article/details/115514937?spm=1001.2101.3001.6650.2&utm_medium=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7ERate-2.pc_relevant_antiscanv2&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2%7Edefault%7ECTRLIST%7ERate-2.pc_relevant_antiscanv2&utm_relevant_index=5

```

#### 介绍
爬取东方财富数据中心资金流向

#### 计划
1. 爬取每天所有股票资金流向数据存入数据库
2. 将每日涨幅>7%股票筛选出来
3. 将筛选出来的股票分析出每天资金流向 

#### 依赖库
+ requests
+ json
+ jsonpath
+ pandas
+ openpyxl
+ altair
+ pymongo
#### 爬取结果
![alt text](./img/img.jpg)

#### 简单数据可视化
![1](./img/1.png)   
![2](./img/2.png)   
![3](./img/3.png)   

**导入数据库（mongodb）**

![mongodb](./img/mongodb.PNG)

#### 接口说明
```python
pn和pz，并且经过测试后发现这确实是用于表示页数和页大小

```
